import train_args, test_args, test
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from tqdm import tqdm
import pickle


if __name__ == '__main__':

    #TuneID
    ID = 6

    # Network configuration
    batch_size = 10
    epochs = 100

    # Data
    dataID = 18
    train_size = 8000
    test_size = 3000


    #different beta values
    betas = [25, 75, 125, 175]
    trainID = 200 #the IDs the train cases are saved for during tuning
    testID = 200 #the IDs the test cases are saved for during tuning

    dir = "beta_tuning/"

    trans_mean = []
    rot_mean = []
    trans_med = []
    rot_med = []


    for i, beta in enumerate(tqdm(betas)):
        trainID += 1
        testID += 1
        train_args.main(trainID=trainID, batch_size=batch_size, epochs=epochs, dataID=dataID, train_size=train_size, beta=beta)

        res=test_args.main(testID=testID, dataID=dataID, test_size=test_size, trainID=trainID)
        print(res)
        trans_mean.append(res[0])
        rot_mean.append(res[1])
        trans_med.append(res[2])
        rot_med.append(res[3])


        plt.figure(100)
        plt.plot(betas[:i+1], np.array(trans_mean)*100, label="Mean translation error[cm]")
        plt.plot(betas[:i+1], np.array(rot_mean), label="Mean orientation error[deg]")
        plt.plot(betas[:i + 1], np.array(trans_med) * 100, label="Median translation error[cm]")
        plt.plot(betas[:i + 1], np.array(rot_med), label="Median orientation error[deg]")
        plt.xlabel("Scaling factor beta")
        plt.legend()
        plt.savefig(dir + 'tuning{}.eps'.format(ID), format='eps', dpi=1000)
        plt.close()
        if i > 1:
            plt.figure(101)
            plt.plot(betas[1:i+1], np.array(trans_mean)[1:]*100, label ="Mean translation error[cm]")
            plt.plot(betas[1:i+1], np.array(rot_mean)[1:], label="Mean orientation error [deg]")
            plt.plot(betas[1:i + 1], np.array(trans_med)[1:] * 100, label="Median translation error[cm]")
            plt.plot(betas[1:i + 1], np.array(rot_med)[1:], label="Median orientation error [deg]")

            plt.xlabel("Scaling factor beta")
            plt.legend()
            plt.savefig(dir + 'tuning{}.eps'.format(ID), format='eps', dpi=1000)
            plt.close()

        data = [trans_mean, rot_mean, trans_med, rot_med, betas]
        with open(dir + 'tuning{}.pkl'.format(ID), 'wb') as fid:
            pickle.dump(data, fid)


