# Import the converted model's class
import numpy as np
import random
import tensorflow as tf
from posenet import GoogLeNet as PoseNet
import cv2
from tqdm import tqdm
import pickle
import time
import h5py
import os
import errno
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import itertools


# ------------------------ Settings --------------------------------------------------------------
trainID = 47

# Network configuration
batch_size = 10
epochs = 150
beta = 75

# Data
dataID = 56
train_size = 11000

# Only if data is read from txt file and folder: (not from hdf files)
from_text_file = False
text_directory = '../poselab/docker/resources/'
text_file = 'train_py3.pkl'
# -------------------------------------------------------------------------------------------------



# Default
preprocess = "resized and cropped at center, + pad"
no_batches = int(train_size/batch_size)
no_val_batches = int(0.2*no_batches)

directory = '../../hdf_data/'
pickle_file = "set#{}_.pkl".format(dataID)
train_file = 'set#{}_train_data.h5'.format(dataID)
test_file = 'set#{}_test_data.h5'.format(dataID)
outputFile = "PoseNet{}.ckpt".format(trainID)

load_weights = 'None'
description = """
    Train #{}
    Dataset #{}
    Size of training set: {} 
    Preprocessing option: {}
    Train_file = {}
    Output weights = {}
    Transfer learning: Googlenet, places
    Loaded from weight file: {}
    Training started: {}
    
    Batch size: {} 
    Epochs: {}
    Beta: {}
    """.format(trainID, dataID, train_size, preprocess, directory + train_file, outputFile, load_weights, datetime.datetime.now(), batch_size, epochs, beta )

dir = 'train#' + str(trainID) + '/'
if not os.path.exists(os.path.dirname(dir)):
	try:
		os.makedirs(os.path.dirname(dir))
	except OSError as exc:  # Guard against race condition
		if exc.errno != errno.EEXIST:
			raise

with open(dir + 'train_description.txt', "w") as f:
	f.write(description)
	f.close()

print("Starts training #{}, on dataset #{} of size {} images".format(trainID, dataID, train_size))
# ----------------------------------------------------------------------------------------------------


class Datasource(object):
	def __init__(self, images, poses):
		self.images = images
		self.poses = poses


def centeredCrop(img, output_side_length):
	""" Crop images at center"""
	height, width, depth = img.shape
	new_height = output_side_length
	new_width = output_side_length
	if height > width:
		new_height = output_side_length * height / width
	else:
		new_width = output_side_length * width / height
	height_offset = (new_height - output_side_length) / 2
	width_offset = (new_width - output_side_length) / 2
	cropped_img = img[height_offset:height_offset + output_side_length,
						width_offset:width_offset + output_side_length]
	return cropped_img


def preprocess(images):
	""" Crop and resize images"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):
		X = cv2.resize(images[i], (455, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped


def preprocess_pad(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):
		X = images[i]
		pad = int((X.shape[1]-X.shape[0])/2)
		X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		X = cv2.resize(X, (256, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped


def preprocess_pad_png(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):

		X = cv2.imread(images[i])
		pad = int((X.shape[1]-X.shape[0])/2)
		X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		X = cv2.resize(X, (256, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped


def get_data_batch(directory, filename, start_index, batch_size):
	""" Reads a batch of data, returns a datasource instancce"""
	poses = []
	images = []
	with h5py.File(directory + filename, 'r') as hf:
		for i in range(start_index, start_index + batch_size):
			datapoint = hf["data" + str(i)]
			images.append(datapoint[()])
			pose = datapoint.attrs["pose"]
			poses.append(pose)
	images_out = preprocess_pad(images)
	return Datasource(images_out, poses)

def get_data_batch_vec(directory, filename, sample_vec):
	""" Reads a batch of data, returns a datasource instancce. Selects specific instances given vector..."""
	poses = []
	images = []
	with h5py.File(directory + filename, 'r') as hf:
		for i in sample_vec:
			datapoint = hf["data" + str(i)]
			images.append(datapoint[()])
			pose = datapoint.attrs["pose"]
			poses.append(pose)
	images_out = preprocess_pad(images)
	return Datasource(images_out, poses)

def get_data_batch_from_txt(directory, filename, start_index, batch_size):
	poses = []
	images = []

	with open(directory + filename) as f:
		next(f)  # skip the 3 header lines
		next(f)
		next(f)
		for line in itertools.islice(f, start_index, start_index + batch_size):
			fname, p0, p1, p2, p3, p4, p5, p6 = line.split()
			p0 = float(p0)
			p1 = float(p1)
			p2 = float(p2)
			p3 = float(p3)
			p4 = float(p4)
			p5 = float(p5)
			p6 = float(p6)
			poses.append((p0, p1, p2, p3, p4, p5, p6))
			images.append(directory + fname)
	images = preprocess_pad(images)
	return Datasource(images, poses)

def get_data_batch_from_pickle(directory, picklefile, start_index, batch_size):
	poses = []
	images = []

	with open(directory + picklefile, 'rb') as f:
		res = pickle.load(f)
		f.close()

	img_dir = 'tracking_data/'
	for i in range(start_index, start_index + batch_size):
		fname = res['ID'][i]
		pos = np.array(res['tvec'][i])
		orient = np.array(res['rvec'][i])
		pose = np.concatenate((pos, orient))
		poses.append(pose)
		images.append(directory + img_dir + fname)
	images = preprocess_pad_png(images)
	return Datasource(images, poses)


def main(beta=beta):

	images = tf.placeholder(tf.float32, [batch_size, 224, 224, 3])
	poses_x = tf.placeholder(tf.float32, [batch_size, 3])
	poses_q = tf.placeholder(tf.float32, [batch_size, 4])

	net = PoseNet({'data': images})

	p1_x = net.layers['cls1_fc_pose_xyz']
	p1_q = net.layers['cls1_fc_pose_wpqr']
	p2_x = net.layers['cls2_fc_pose_xyz']
	p2_q = net.layers['cls2_fc_pose_wpqr']
	p3_x = net.layers['cls3_fc_pose_xyz']
	p3_q = net.layers['cls3_fc_pose_wpqr']

	l1_x = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(p1_x, poses_x)))) * 0.3
	l1_q = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(p1_q, poses_q)))) * 0.3*beta
	l2_x = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(p2_x, poses_x)))) * 0.3
	l2_q = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(p2_q, poses_q)))) * 0.3*beta
	l3_x = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(p3_x, poses_x)))) * 1
	l3_q = tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(p3_q, poses_q)))) * beta

	loss = l1_x + l1_q + l2_x + l2_q + l3_x + l3_q
	opt = tf.train.AdamOptimizer(learning_rate=0.0001, beta1=0.9, beta2=0.999, epsilon=0.00000001, use_locking=False, name='Adam').minimize(loss)

	# Set GPU options
	gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6833)

	init = tf.global_variables_initializer()
	saver = tf.train.Saver()

	with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
		# Load the data
		sess.run(init)

		# Load the weights obtained with GoogLeNet on the Places dataset
		net.load('posenet.npy', sess)
		#saver.restore(sess, "PoseNet.ckpt")


		start = time.time()
		start0 = time.time()
		train_costs = []
		val_costs = []

		for i in range(epochs):
			print("epoch {}/{}".format(i, epochs))
			avg_train_cost = 0
			avg_val_cost = 0
			vec = random.sample(range(no_batches), no_batches)
			shuffle_vec = random.sample(range(train_size), train_size)
			#todo: seed ?
			train_batches = vec[:no_batches-no_val_batches]
			val_batches = vec[no_batches-no_val_batches:]

			for j, k in enumerate(train_batches):
				if not from_text_file:
					#datasource = get_data_batch(directory, train_file, k*batch_size, batch_size)
					samples = shuffle_vec[k*batch_size:k*batch_size + batch_size]
					datasource = get_data_batch_vec(directory, train_file, samples)

				else:
					print(k)
					datasource = get_data_batch_from_pickle(text_directory, text_file, k*batch_size, batch_size)

				np_images = datasource.images
				np_poses_x = [pose[:3] for pose in datasource.poses]
				np_poses_q = [pose[3:] for pose in datasource.poses]

				feed = {images: np_images, poses_x: np_poses_x, poses_q: np_poses_q}

				sess.run(opt, feed_dict=feed)
				np_loss = sess.run(loss, feed_dict=feed)
				avg_train_cost += np_loss / no_batches
				if j % 20 == 0:
					print("iteration: " + str(j) + "\n\t" + "Loss is: " + str(np_loss))
					stop = time.time()
					duration = stop - start
					print("Time elapsed: {0}".format(duration))
					start = time.time()
				if (i*max(0,j-1) + j) % 5000 == 0:
					saver.save(sess, "./" + dir + outputFile)
					print("Intermediate file saved at: " + dir + outputFile)

			print("calculating validation loss for epoch {}...".format(i))
			for j, k in enumerate(val_batches):
				if not from_text_file:
					datasource = get_data_batch(directory, train_file, k*batch_size, batch_size)
					samples = shuffle_vec[k * batch_size:k * batch_size + batch_size]
					datasource = get_data_batch_vec(directory, train_file, samples)
				else:
					datasource = get_data_batch_from_pickle(text_directory, text_file, k*batch_size, batch_size)
				np_images = datasource.images
				np_poses_x = [pose[:3] for pose in datasource.poses]
				np_poses_q = [pose[3:] for pose in datasource.poses]

				feed = {images: np_images, poses_x: np_poses_x, poses_q: np_poses_q}

				np_loss = sess.run(loss, feed_dict=feed)
				avg_val_cost += np_loss / no_val_batches

			train_costs.append(avg_train_cost)
			val_costs.append(avg_val_cost)
		plt.figure(1)
		plt.plot(train_costs, label='training loss')
		plt.plot(val_costs, label='validation loss')
		plt.legend(loc='upper right')
		plt.xlim(0, epochs)
		plt.xlabel("Epochs")
		plt.title("Training and validation loss over epochs, on training ID #{}".format(trainID))
		plt.savefig("./" + dir + "validation{}.eps".format(trainID), format='eps', dpi=1000)


		saver.save(sess, "./" + dir + outputFile)
		end = time.time()
		duration = (end - start0)/3600
		print("Intermediate file saved at: " + dir + outputFile)
		print("Training lasted for {} hours".format(duration))


if __name__ == '__main__':
	main()
