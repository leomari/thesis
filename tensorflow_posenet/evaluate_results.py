#!/usr/bin/env python
# import poselab
import cv2
import os
import argparse
import numpy as np
import csv
import logging
import pickle
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats


# ---------- Define test ID and accuracy requirements here ----------
ID = 86


percentage = True # If true, the requirements below serve as ammounts of the relative pos/orient in the label.
#If false, they are given in meters and degrees
translation_requirement = None
rotation_requirement = 0.1


dir = 'results/test#{}/'.format(ID) # The directory where the results are stored


def rad_to_deg(rad):
    return rad*180.0/np.pi


def wrap2Pi(angle):
    return np.arctan2(np.sin(angle), np.cos(angle))


def normalize(q):
    return q/np.linalg.norm(q)

def qinv(q):
    """ Inverts a Quaternion """

    assert q.size == 4

    q[1:] = -q[1:]
    q = q/np.linalg.norm(q)

    return q

def Smtrx(x):
    """ Setup a Skew-Symmetrix Matrix """
    assert x.size == 3
    return np.array([
        [0, -x[2], x[1]],
        [x[2], 0, -x[0]],
        [-x[1], x[0], 0]])

def quaternion_multiplication(a, b):
    """ Calculates the quaternion product between two quaternions """
    a0 = a[0]
    a1 = a[1:]
    b0 = b[0]
    b1 = b[1:]
    logging.debug('a0, type: {0}, value: {1}'.format(type(a0), a0))
    logging.debug('a1, type: {0}, value: {1}'.format(type(a1), a1))
    logging.debug('b0, type: {0}, value: {1}'.format(type(b0), b0))
    logging.debug('b1, type: {0}, value: {1}'.format(type(b1), b1))
    v0 = a0*b0-np.dot(a1, b1)
    v1 = a0*b1+a1*b0-np.dot(Smtrx(a1), b1)
    return np.insert(v1, 0, v0)


def get_posenet_error(q1, q2):
    """ corresponds to: theta =  2*acos(|z_0|), where z = q1*inv(q2) """

    ang = 2 * np.arccos(abs(np.dot(q1, q2)) / (np.linalg.norm(q1) * np.linalg.norm(q2)))
    return ang * 180 / np.pi


def to_xyzw(quat):
    w,x,y,z = quat
    return np.array([x, y, z, w])

def to_wxyz(quat):
    x,y,z,w = quat
    return np.array([w, x, y, z])



if __name__ == '__main__':

    
    with open(dir + 'results.pkl', 'rb') as f:
        results = pickle.load(f)#, encoding='latin1')

    blender_results = results['true']
    aruco_results = results['pred']

    # Euler Angle difference:

    ea_errors = []
    tvec_errors = []
    rot_errors = []
    trans_errors = []
    xyz_errors = np.array([0,0,0])
    rpy_errors = np.array([0,0,0])
    trans_ammount = []
    rot_ammount = []
    rotations = []
    translations = []
    index = 0
    for b_ea, a_ea in zip(blender_results['ea'], aruco_results['ea']):

        error_tvec = (blender_results['tvec'][index] - aruco_results['tvec'][index])
        error_euler = (b_ea - a_ea)*180/np.pi
        xyz_errors = np.vstack([xyz_errors, error_tvec])
        rpy_errors = np.vstack([rpy_errors, error_euler])
        error_rot = get_posenet_error(blender_results['q'][index], aruco_results['q'][index])
        error_trans = np.linalg.norm(error_tvec)
        if any(abs(rad_to_deg(error_euler)) > 180.0):
            error_euler = np.array([wrap2Pi(x) for x in error_euler.tolist()])

        ea_errors.append(error_euler)
        tvec_errors.append(error_tvec)
        rot_errors.append(error_rot)
        trans_errors.append(error_trans)

        rot = get_posenet_error(np.array([1, 0, 0, 0]), aruco_results['q'][index])
        trans = np.linalg.norm(blender_results['tvec'][index])
        trans_ammount.append(float(error_trans)/trans)
        rot_ammount.append(float(error_rot)/rot)
        rotations.append(rot)
        translations.append(trans)

        index += 1

    true_trans = blender_results['tvec']
    true_rot = np.array(blender_results['ea'])*180/np.pi
    pred_trans = aruco_results['tvec']
    pred_rot =  np.array(aruco_results['ea'])*180/np.pi



    ea_errors = np.array(ea_errors)
    tvec_errors = np.array(tvec_errors)
    abs_ea_errors = np.abs(ea_errors)
    abs_tvec_errors = np.abs(tvec_errors)

    roll_error = ea_errors[:, 0]
    pitch_error = ea_errors[:, 1]
    yaw_error = ea_errors[:, 2]
    x_error = tvec_errors[:, 0]
    y_error = tvec_errors[:, 1]
    z_error = tvec_errors[:, 2]

    roll_error.sort()
    pdf_roll = stats.norm.pdf(roll_error, np.mean(roll_error), np.std(roll_error))
    pitch_error.sort()
    pdf_pitch = stats.norm.pdf(pitch_error, np.mean(pitch_error), np.std(pitch_error))
    yaw_error.sort()
    pdf_yaw = stats.norm.pdf(yaw_error, np.mean(yaw_error), np.std(yaw_error))
    x_error.sort()
    pdf_x = stats.norm.pdf(x_error, np.mean(x_error), np.std(x_error))
    y_error.sort()
    pdf_y = stats.norm.pdf(y_error, np.mean(y_error), np.std(y_error))
    z_error.sort()
    pdf_z = stats.norm.pdf(z_error, np.mean(z_error), np.std(z_error))





    abs_roll_error = abs_ea_errors[:, 0]
    abs_pitch_error = abs_ea_errors[:, 1]
    abs_yaw_error = abs_ea_errors[:, 2]
    abs_x_error = abs_tvec_errors[:, 0]
    abs_y_error = abs_tvec_errors[:, 1]
    abs_z_error = abs_tvec_errors[:, 2]


    #Number of correct samples
    total = len(tvec_errors)
    number=0
    if translation_requirement and rotation_requirement:
        print("both")
        for i in range(total):
            if percentage:
                if trans_ammount[i] <= translation_requirement and rot_ammount[i] <= rotation_requirement:
                    number += 1
            else:
                if trans_errors[i] <= translation_requirement and rot_errors[i] <= rotation_requirement:
                    number += 1
    elif translation_requirement:
        if percentage:
            number = sum(i <= translation_requirement for i in trans_ammount)
        else:
            number = sum(i <= translation_requirement for i in trans_errors)
    elif rotation_requirement:
        if percentage:
            number = sum(i <= rotation_requirement for i in rot_ammount)
        else:
            number = sum(i <= rotation_requirement for i in rot_errors)
    else:
        print("no requirement for correctness")
        number = total
    correct = float(number)/total*100


    #Metrics
    undetected = results['undetected']
    size = len(blender_results['tvec'])
    avg_trans = [np.mean(res) for res in [tvec_errors[:, i] for i in range(3)]]
    avg_rot = [np.mean(res) for res in [ea_errors[:, i] for i in range(3)]]
    avg_rot = [np.mean(res) for res in [rpy_errors[:, i] for i in range(3)]]
    std_trans = [np.std(res) for res in [tvec_errors[:, i] for i in range(3)]]
    std_rot = [np.std(res) for res in [ea_errors[:, i] for i in range(3)]]
    std_rot = [np.std(res) for res in [rpy_errors[:, i] for i in range(3)]]



    abs_avg_trans = [np.mean(res) for res in [abs_tvec_errors[:, i] for i in range(3)]]
    abs_avg_rot = [np.mean(res) for res in [abs_ea_errors[:, i] for i in range(3)]]



    avg_rel_rot = np.mean(rot_errors)
    euc_trans = np.mean(trans_errors)
    med_rot = np.median(rot_errors)
    med_trans = np.median(trans_errors)
    std_rel_rot = np.std(rot_errors)
    std_euc = np.std(trans_errors)



    data = """
    Number of undetected markers: {0} out of {1} samples.
    
    Average errors: \t \t Standard deviation: 
    x: {2[0]:.5f} m \t \t {11[0]:.5f} m
    y: {2[1]:.5f} m \t \t {11[1]:.5f} m
    z: {2[2]:.5f} m \t \t {11[2]:.5f} m
    roll: {3[0]:.3f} deg \t \t {12[0]:.3f} deg
    pitch: {3[1]:.3f} deg \t \t {12[1]:.3f} deg
    yaw: {3[2]:.3f} deg \t \t {12[2]:.3f} deg
    
    
    Average absolute value errors: 
    x: {6[0]:.4f} m
    y: {6[1]:.4f} m
    z: {6[2]:.4f} m
    roll: {7[0]:.4f} deg
    pitch: {7[1]:.4f} deg
    yaw: {7[2]:.4f} deg
    
    
    Average relative angle between predicted and true rotation: {4:.4f} deg
    Average Eucledian distance between predicted and true translation: {5:.5f} m
    
    Median relative angle between predicted and true rotation: {9:.4f} deg 
    Median Eucledian distance between predicted and true translation: {10:.5f} m
    
    Std deviation relative angle: {13:.4f} deg
    Std deviation Euclidean distance: {14:.5f} m 
    
    Correct pose estimates: {8:.2f} percentage
    """.format(undetected, size, avg_trans, avg_rot, avg_rel_rot, euc_trans, abs_avg_trans, abs_avg_rot,
               correct, med_rot, med_trans, std_trans, std_rot, std_rel_rot, std_euc)

    with open(dir + "test{}_".format(ID) + 'description.txt', 'a') as f:
        f.write(data)
        f.close()

    print(data)

    plt.figure(0, figsize=(13, 20))

    plt.subplot(321)
    plt.hist(rpy_errors[:, 0], bins=100, edgecolor='k')
    plt.xlabel("roll error [deg]")
    plt.ylabel("number of samples")

    plt.subplot(322)
    plt.hist(rpy_errors[:, 1], bins=100, edgecolor='k')
    plt.xlabel("pitch error [deg]")
    plt.ylabel("number of samples")

    plt.subplot(323)
    plt.hist(rpy_errors[:, 2], bins=100, edgecolor='k')
    plt.xlabel("yaw error [deg]")
    plt.ylabel("number of samples")

    plt.subplot(324)
    plt.hist(x_error*1000, bins=100, edgecolor='k')
    plt.xlabel("surge error [mm]")
    plt.ylabel("number of samples")
    plt.subplot(325)
    plt.hist(y_error*1000, bins=100, edgecolor='k')
    plt.xlabel("sway error [mm]")
    plt.ylabel("number of samples")
    plt.subplot(326)
    plt.hist(z_error*1000, bins=100, edgecolor='k')
    plt.xlabel("heave error [mm]")
    plt.ylabel("number of samples")
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_0.eps', format='eps', dpi=1000)


    plt.figure(1, figsize=(13, 20))

    plt.subplot(321)
    plt.hist(roll_error, bins=100, edgecolor='k')
    plt.plot(roll_error, pdf_roll)
    plt.xlabel("roll error [deg]")
    plt.ylabel("number of samples")

    plt.subplot(322)
    plt.hist(pitch_error, bins=100, edgecolor='k')
    plt.plot(pitch_error, pdf_pitch)
    plt.xlabel("pitch error [deg]")
    plt.ylabel("number of samples")

    plt.subplot(323)
    plt.hist(yaw_error, bins=100, edgecolor='k')
    plt.plot(yaw_error, pdf_yaw)
    plt.xlabel("yaw error [deg]")
    plt.ylabel("number of samples")

    plt.subplot(324)
    plt.hist(x_error, bins=100, edgecolor='k')
    plt.plot(x_error, pdf_x)
    plt.xlabel("surge error [m]")
    plt.ylabel("number of samples")
    plt.subplot(325)
    plt.hist(y_error, bins=100, edgecolor='k')
    plt.plot(y_error, pdf_y)
    plt.xlabel("sway error [m]")
    plt.ylabel("number of samples")
    plt.subplot(326)
    plt.hist(z_error, bins=100, edgecolor='k')
    plt.plot(z_error, pdf_z)
    plt.xlabel("heave error [m]")
    plt.ylabel("number of samples")
    plt.savefig(dir + "test{}_".format(ID) +'Pose_res_2.eps', format='eps', dpi=1000)

    plt.figure(2, figsize=(13, 20))

    plt.subplot(322)
    plt.hist(abs_roll_error, bins=50, edgecolor='k')
    plt.xlabel("absolute roll error [deg]")
    plt.ylabel("number of samples")
    plt.subplot(323)
    plt.hist(abs_pitch_error, bins=50, edgecolor='k')
    plt.xlabel("absolute pitch error [deg]")
    plt.ylabel("number of samples")
    plt.subplot(321)
    plt.hist(abs_yaw_error, bins=50, edgecolor='k')
    plt.xlabel("absolute yaw error [deg]")
    plt.ylabel("number of samples")
    plt.subplot(325)
    plt.hist(abs_x_error, bins=50, edgecolor='k')
    plt.xlabel("absolute surge error [m]")
    plt.ylabel("number of samples")
    plt.subplot(326)
    plt.hist(abs_y_error, bins=50, edgecolor='k')
    plt.xlabel("absolute sway error [m]")
    plt.ylabel("number of samples")
    plt.subplot(324)
    plt.hist(abs_z_error, bins=50, edgecolor='k')
    plt.xlabel("absolute heave error [m]")
    plt.ylabel("number of samples")
    plt.savefig(dir + "test{}_".format(ID) +'Pose_res_3.eps', format='eps', dpi=1000)

    trans_errors_mm = 1000*np.array(trans_errors)
    plt.figure(3, figsize=(10, 5))
    plt.subplot(121)
    plt.hist(rot_errors, bins=100, edgecolor='k')
    plt.xlabel("angle error [deg]")
    plt.ylabel("number of samples")
    plt.subplot(122)
    plt.hist(trans_errors_mm, bins=100, edgecolor='k')
    plt.xlabel("Euclidean translational error [mm]")
    plt.ylabel("number of samples")
    plt.savefig(dir + "test{}_".format(ID) +'Pose_res_1.eps', format='eps', dpi=1000)
    #plt.show()

    plt.figure(4, figsize=(10, 5))
    plt.subplot(221)
    plt.hist(rot_ammount, bins = 50, edgecolor = 'k')
    plt.xlabel("Ammount of rotation error")
    plt.subplot(222)
    plt.hist(trans_ammount, bins=50, edgecolor='k')
    plt.xlabel("Ammount of translation error")
    plt.subplot(223)
    plt.hist(rotations, bins=50, edgecolor='k')
    plt.xlabel("rotations")
    plt.subplot(224)
    plt.hist(translations, bins = 50, edgecolor='k')
    plt.xlabel("translations")
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_5.eps', format='eps', dpi=1000)

    plt.figure(5)
    plt.scatter(rot_errors, trans_errors, s = 1)
    plt.xlabel('Rotation errors [deg]')
    plt.ylabel('Translation errors [m]')
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_6.eps', format='eps', dpi=1000)

    plt.figure(6, figsize=(10,15))
    plt.subplot(311)
    plt.plot(np.array(true_trans)[:, 0], label="True position", linewidth=0.1)
    plt.plot(np.array(pred_trans)[:, 0], label="Predicted position", linewidth=0.1)
    plt.ylabel("[m]")
    plt.xlabel("Samples")
    plt.title("X direction")
    plt.legend()

    plt.subplot(312)
    plt.plot(np.array(true_trans)[:, 1], label="True position", linewidth=0.1)
    plt.plot(np.array(pred_trans)[:, 1], label="Predicted position", linewidth=0.1)
    plt.ylabel("[m]")
    plt.xlabel("Samples")
    plt.title("Y direction")
    plt.legend()

    plt.subplot(313)
    plt.plot(np.array(true_trans)[:, 2], label="True position", linewidth=0.1)
    plt.plot(np.array(pred_trans)[:, 2], label="Predicted position", linewidth=0.1)
    plt.ylabel("[m]")
    plt.xlabel("Samples")
    plt.title("Z direction")
    plt.legend()

    plt.suptitle("True and predicted position")
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_7.eps', format='eps', dpi=1000)

    plt.figure(7, figsize=(10,15))
    plt.subplot(311)
    plt.plot(true_rot[:, 0], label="True orientation", linewidth=0.1)
    plt.plot(pred_rot[:, 0], label="Predicted orientation", linewidth=0.1)
    plt.ylabel("[deg]")
    plt.xlabel("Samples")
    plt.title("Roll direction")
    plt.legend()

    plt.subplot(312)
    plt.plot(true_rot[:, 1], label="True orientation", linewidth=0.1)
    plt.plot(pred_rot[:, 1], label="Predicted orientation", linewidth=0.1)
    plt.ylabel("[deg]")
    plt.xlabel("Samples")
    plt.title("Pitch direction")
    plt.legend()

    plt.subplot(313)
    plt.plot(true_rot[:, 2], label="True orientation", linewidth=0.1)
    plt.plot(pred_rot[:, 2], label="Predicted orientation", linewidth=0.1)
    plt.ylabel("[deg]")
    plt.xlabel("Samples")
    plt.title("Yaw direction")
    plt.legend()
    plt.suptitle("True and predicted orientation")
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_8.eps', format='eps', dpi=1000)

    plt.figure(8, figsize=(10, 15))
    plt.subplot(311)
    plt.plot(xyz_errors[:, 0], linewidth=0.1)
    plt.ylabel("Error [m]")
    plt.xlabel("Samples")
    plt.title("X direction")

    plt.subplot(312)
    plt.plot(xyz_errors[:, 1], linewidth=0.1)
    plt.ylabel("Error [m]")
    plt.xlabel("Samples")
    plt.title("Y direction")

    plt.subplot(313)
    plt.plot(xyz_errors[:, 2], linewidth=0.1)
    plt.ylabel("Error [m]")
    plt.xlabel("Samples")
    plt.title("Z direction")

    plt.suptitle("Position errors")
    plt.savefig(dir + "test{}_".format(ID) + 'Position_errors.eps', format='eps', dpi=1000)

    plt.figure(9, figsize=(10, 15))
    plt.subplot(311)
    plt.plot(rpy_errors[:, 0], linewidth=0.1)
    plt.ylabel("Error [deg]")
    plt.xlabel("Samples")
    plt.title("Roll direction")

    plt.subplot(312)
    plt.plot(rpy_errors[:, 1], linewidth=0.1)
    plt.ylabel("Error [deg]")
    plt.xlabel("Samples")
    plt.title("Pitch direction")

    plt.subplot(313)
    plt.plot(rpy_errors[:, 2], linewidth=0.1)
    plt.ylabel("Error [deg]")
    plt.xlabel("Samples")
    plt.title("Yaw direction")

    plt.suptitle("Orientation errors")
    plt.savefig(dir + "test{}_".format(ID) + 'Orientation_errors.eps', format='eps', dpi=1000)

    plt.figure(10, figsize=(10, 15))
    plt.subplot(311)
    plt.plot(true_rot[:, 0], linewidth=0.1)
    plt.ylabel("[deg]")
    plt.xlabel("Samples")
    plt.title("Roll direction")

    plt.subplot(312)
    plt.plot(true_rot[:, 1], linewidth=0.1)
    plt.ylabel("[deg]")
    plt.xlabel("Samples")
    plt.title("Pitch direction")

    plt.subplot(313)
    plt.plot(true_rot[:, 2], linewidth=0.1)
    plt.ylabel("[deg]")
    plt.xlabel("Samples")
    plt.title("Yaw direction")
    plt.suptitle("True orientations")
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_orients.eps', format='eps', dpi=1000)

    plt.figure(11, figsize=(10, 15))
    plt.subplot(311)
    plt.plot(np.array(true_trans)[:, 0], linewidth=0.1)
    plt.ylabel("[m]")
    plt.xlabel("Samples")
    plt.title("X direction")

    plt.subplot(312)
    plt.plot(np.array(true_trans)[:, 1], linewidth=0.1)
    plt.ylabel("[m]")
    plt.xlabel("Samples")
    plt.title("Y direction")

    plt.subplot(313)
    plt.plot(np.array(true_trans)[:, 2],  linewidth=0.1)
    plt.ylabel("[m]")
    plt.xlabel("Samples")
    plt.title("Z direction")

    plt.suptitle("True position")
    plt.savefig(dir + "test{}_".format(ID) + 'Pose_res_translations.eps', format='eps', dpi=1000)





    plt.figure(12, figsize=(10, 20))
    plt.subplot(611)
    #plt.plot(true_rot[:, 0], linewidth=0.1, label="true pose")
    plt.plot(rpy_errors[:, 0], linewidth=0.1, label="error")
    plt.ylabel("[deg]")
    #plt.xlabel("Samples")
    plt.title("Roll direction")

    plt.subplot(612)
    #plt.plot(true_rot[:, 1], linewidth=0.1, label = "true pose")
    plt.plot(rpy_errors[:, 1], linewidth=0.1, label="error")
    plt.ylabel("[deg]")
    #plt.xlabel("Samples")
    plt.title("Pitch direction")

    plt.subplot(613)
    #plt.plot(true_rot[:, 2], linewidth=0.1, label = "true pose")
    plt.plot(rpy_errors[:, 2], linewidth=0.1, label = "error")
    plt.ylabel("[deg]")
    #plt.xlabel("Samples")
    plt.title("Yaw direction")

    plt.subplot(614)
   # plt.plot(np.array(true_trans)[:, 0], linewidth=0.1, label="true pose")
    plt.plot(xyz_errors[:, 0]*1000, linewidth=0.1, label="error")
    plt.ylabel("[mm]")
    #plt.xlabel("Samples")
    plt.title("X direction")

    plt.subplot(615)
    #plt.plot(np.array(true_trans)[:, 1], linewidth=0.1, label="true pose")
    plt.plot(xyz_errors[:, 1]*1000, linewidth=0.1, label="error")
    plt.ylabel("[mm]")
    #plt.xlabel("Samples")
    plt.title("Y direction")

    plt.subplot(616)
    #plt.plot(np.array(true_trans)[:, 2], linewidth=0.1, label = "true pose")
    plt.plot(xyz_errors[:, 2]*1000, linewidth=0.1, label="error")
    plt.ylabel("[mm]")
    #plt.xlabel("Samples")
    plt.title("Z direction")

    plt.xlabel("samples")

    plt.savefig(dir + "test{}_".format(ID) + 'error_plots.eps', format='eps', dpi=1000)
