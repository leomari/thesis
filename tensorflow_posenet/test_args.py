# Import the converted model's class
import numpy as np
import random
import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
from posenet import GoogLeNet as PoseNet
import cv2
from tqdm import tqdm
import math
import pickle
import h5py
import os
import errno
import time
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

""" duplicate of test.py to be used for betatuning """



class Datasource(object):
	def __init__(self, images, poses):
		self.images = images
		self.poses = poses


class vecsource(object):
	def __init__(self, vecs, poses):
		self.vecs = vecs
		self.poses = poses


def quat_to_euler(q):
    roll = np.arctan2(2*(q[0]*q[1] + q[2]*q[3]), 1 - 2*(q[1]**2 + q[2]**2))
    pitch = np.arcsin(2*(q[0]*q[2]) - q[3]*q[1])
    yaw = np.arctan2(2*(q[0]*q[3] + q[1]*q[2]), 1 - 2*(q[2]**2 + q[3]**2))

    return np.array([roll, pitch, yaw])


def centeredCrop(img, output_side_length):
	height, width, depth = img.shape
	new_height = output_side_length
	new_width = output_side_length
	if height > width:
		new_height = output_side_length * height / width
	else:
		new_width = output_side_length * width / height
	height_offset = (new_height - output_side_length) / 2
	width_offset = (new_width - output_side_length) / 2
	cropped_img = img[height_offset:height_offset + output_side_length,
						width_offset:width_offset + output_side_length]
	return cropped_img


def preprocess(images):
	""" Crop and resize images"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):
		X = cv2.resize(images[i], (455, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped


def preprocess_pad(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):
		X = images[i]
		pad = int((X.shape[1]-X.shape[0])/2)
		X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		X = cv2.resize(X, (256, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped




def preprocess_pad_png(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):

		X = cv2.imread(images[i])
		pad = int((X.shape[1]-X.shape[0])/2)
		X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		X = cv2.resize(X, (256, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped


def get_data(directory, filename):
	""" Reads all the test data, returns a datasource instancce"""
	poses = []
	images = []
	with h5py.File(directory + filename, 'r') as hf:
		for i in range(len(hf.keys()) - 1):
			datapoint = hf["data" + str(i)]
			images.append(datapoint[()])
			pose = datapoint.attrs["pose"]
			poses.append(pose)
	images_out = preprocess_pad(images)
	return Datasource(images_out, poses)


def get_data_from_pickle(directory, picklefile):
	poses = []
	images = []

	with open(directory + picklefile, 'rb') as f:
		res = pickle.load(f)
		f.close()

	img_dir = 'tracking_data/'
	for i in range(len(res['ID'])):
		fname = res['ID'][i]
		pos = np.array(res['tvec'][i])
		orient = np.array(res['rvec'][i])
		pose = np.concatenate((pos, orient))
		poses.append(pose)
		images.append(directory + img_dir + fname)
	images = preprocess_pad_png(images)
	return Datasource(images, poses)



def main(testID, dataID, test_size, trainID):
	tf.reset_default_graph()
	# ------------------------ Settings --------------------------------------


	# Only if data is read from txt file and folder: (not from hdf files)
	from_text_file = False
	text_directory = '../poselab/docker/resources/'
	text_file = 'test_py2.pkl'

	# Thresholds
	theta_thresh = 1.0
	translation_thresh = 0.01
	theta_loos_thresh = 10.0
	translation_loos_thresh = 0.15
	save_figures = False

	# --------------------------------------------------------------------------





	# Default
	preprocess = "resized and cropped at center + pad"
	directory = '../../hdf_data/'
	test_file = 'set#{}_test_data.h5'.format(dataID)
	weights = "PoseNet{}.ckpt".format(trainID)
	weight_dir = 'train#{}/'.format(trainID)
	pickle_file = "set#{}_.pkl".format(dataID)
	description = """
	    Test #{}
	    Train #{}
	    Data #{}
	    Quaternion rep: 
	    Number of images: {} 
	    Preprocessing option: {}
	    Datafile = {}
	    Weights = {}
	    """.format(testID, trainID, dataID, test_size, preprocess, directory + test_file, weights)

	# make directory for results
	dir = 'results/test#' + str(testID) + '/'
	img_dir = dir + 'winner_images/'
	looser_dir = dir + 'looser_images/'
	if not os.path.exists(os.path.dirname(dir)):
		try:
			os.makedirs(os.path.dirname(dir))
		except OSError as exc:  # Guard against race condition
			if exc.errno != errno.EEXIST:
				raise

	if not os.path.exists(os.path.dirname(img_dir)):
		try:
			os.makedirs(os.path.dirname(img_dir))
		except OSError as exc:  # Guard against race condition
			if exc.errno != errno.EEXIST:
				raise

	if not os.path.exists(os.path.dirname(looser_dir)):
		try:
			os.makedirs(os.path.dirname(looser_dir))
		except OSError as exc:  # Guard against race condition
			if exc.errno != errno.EEXIST:
				raise

	# Only if data is read from txt file and folder: (not from hdf files)
	from_text_file = False
	text_directory = '../poselab/docker/resources/'
	text_file = 'test_py2.pkl'

	# Thresholds
	theta_thresh = 1.0
	translation_thresh = 0.01
	theta_loos_thresh = 10.0
	translation_loos_thresh = 0.15
	save_figures = False
	# --------------------------------------------------------------------------

	image = tf.placeholder(tf.float32, [1, 224, 224, 3])

	if not from_text_file:
		datasource = get_data(directory, test_file)
	else:
		datasource = get_data_from_pickle(text_directory, text_file)


	res = np.zeros((len(datasource.images),2))

	net1 = PoseNet({'data': image})

	p3_x = net1.layers['cls3_fc_pose_xyz']
	p3_q = net1.layers['cls3_fc_pose_wpqr']

	init = tf.initialize_all_variables()

	saver = tf.train.Saver()

	with tf.Session() as sess:
		# Load the data
		sess.run(init)
		saver.restore(sess, "./" + weight_dir + weights)
		print("stored model {}".format(weights))

		results = {
			'true': {'ID': [], 'q': [], 'ea': [], 'tvec': []},
			'pred': {'ID': [], 'q': [], 'ea': [], 'tvec': []},
			'undetected': []}

		predicted_qs = np.zeros((len(datasource.images), 4))
		predicted_xs = np.zeros((len(datasource.images), 3))

		winners = {'image': [],
				   'rot_error': [],
				   'trans_error': []}
		loosers = {'image': [],
				   'rot_error': [],
				   'trans_error': []}
		counter = 0
		for i in range(len(datasource.images)):
			np_image = [datasource.images[i]]
			feed = {image: np_image}

			pose_q = np.asarray(datasource.poses[i][3:7])
			pose_x = np.asarray(datasource.poses[i][0:3])
			predicted_x, predicted_q = sess.run([p3_x, p3_q], feed_dict=feed)

			pose_q = np.squeeze(pose_q)
			pose_x = np.squeeze(pose_x)
			predicted_q = np.squeeze(predicted_q)
			predicted_x = np.squeeze(predicted_x)

			predicted_xs[i, :] = predicted_x
			predicted_qs[i, :] = predicted_q

			# Add to result dict
			# q = np.asarray(datasource.poses[i][3:7])
			# x = np.asarray(datasource.poses[i][0:3])



			#Compute Individual Sample Error
			q1 = pose_q / np.linalg.norm(pose_q)
			q2 = predicted_q / np.linalg.norm(predicted_q)
			d = abs(np.sum(np.multiply(q1,q2)))
			if abs(d) > 1:
				continue
			theta = 2 * np.arccos(d) * 180/math.pi
			error_x = np.linalg.norm(pose_x-predicted_x)
			res[i, :] = [error_x, theta]
			print('Iteration:  ', i, '  Error XYZ (m):  ', error_x, '  Error Q (degrees):  ', theta)

			results['true']['ID'].append("data_{}".format(i))
			results['true']['q'].append(pose_q)
			results['true']['ea'].append(quat_to_euler(pose_q))
			results['true']['tvec'].append(pose_x)

			results['pred']['ID'].append("data_{}".format(i))
			results['pred']['q'].append(predicted_q)
			results['pred']['ea'].append(quat_to_euler(predicted_q))
			results['pred']['tvec'].append(predicted_x)



			if error_x < translation_thresh: #and theta < theta_thresh
				print("winner")
				winners['image'].append(datasource.images[i])
				winners['rot_error'].append(theta)
				winners['trans_error'].append(error_x)
			if error_x > translation_loos_thresh: #and theta > theta_loos_thresh
				print("looser")
				loosers['image'].append(datasource.images[i])
				loosers['rot_error'].append(theta)
				loosers['trans_error'].append(error_x)
			# if error_x >1.7:
			# 	print("Worst image, translation = ", pose_x - predicted_x, "rotation = ", quat_to_euler(pose_q) -
			# 		  quat_to_euler(predicted_q), "rel_rot = ",  theta, "norm = ", error_x )
			# 	break

	# Write to files
	if save_figures:
		for i in tqdm(range(len(winners['image']))):
			plt.figure(i)
			plt.imshow(winners['image'][i])
			plt.suptitle("rotation error: {0:.4f} degrees, translation error: {1:.4f} m".format(winners['rot_error'][i],
																					  winners['trans_error'][i]))
			plt.savefig(img_dir + 'image{}.png'.format(i), format='png', dpi=1000)
			plt.close(i)
		for i in tqdm(range(len(loosers['image']))):
			plt.figure(i)
			plt.imshow(loosers['image'][i])
			plt.suptitle("rotation error: {0:.4f} degrees, translation error: {1:.4f} m".format(loosers['rot_error'][i],
																								loosers['trans_error'][
																									i]))
			plt.savefig(looser_dir + 'image{}.png'.format(i), format='png', dpi=1000)
			plt.close(i)


	with open(dir + 'results.pkl', 'wb') as f:
		pickle.dump(results, f)
		f.close()

	with open(dir + 'description.txt', "w") as f:
		f.write(description)
		f.close()

	with open(dir + "results.txt", "w") as f:
		f.write("Results from PoseNet \n")

		for i in range(len(datasource.images)):
			pose_q = np.asarray(datasource.poses[i][3:7])
			pose_x = np.asarray(datasource.poses[i][0:3])
			pred_x = predicted_xs[i, :]
			pred_q = predicted_qs[i, :]
			string = """
	image{0}\n
	Predicted position: {1[0]}\t, {1[1]}\t, {1[2]}\n
	True position: {2[0]}\t, {2[1]}\t, {2[2]}\n\n
	Predicted orientation: {3[0]}\t, {3[1]}\t, {3[2]}\t, {3[3]}\n 
	True orientation: {4[0]}\t, {4[1]}\t, {4[2]}\t, {4[3]}\n\n\n
	            """.format(i, pred_x, pose_x, pred_q, pose_q)
			f.write(string)

		f.close()

	median_result = np.median(res, axis=0)
	mean_result = np.mean(res, axis=0)

	print('Median error ', median_result[0], 'm  and ', median_result[1], 'degrees.')
	print('Mean error ', mean_result[0] , 'm and ', mean_result[1] , 'degrees')
	return np.array([mean_result[0], mean_result[1], median_result[0], median_result[1]])


if __name__ == '__main__':
	main(testID=101, dataID=13, test_size=825, trainID=101)
