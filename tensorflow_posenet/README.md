# TensorFlow-PoseNet
**This is an implementation for TensorFlow of the [PoseNet architecture](http://mi.eng.cam.ac.uk/projects/relocalisation/)**

As described in the ICCV 2015 paper **PoseNet: A Convolutional Network for Real-Time 6-DOF Camera Relocalization** Alex Kendall, Matthew Grimes and Roberto Cipolla [http://mi.eng.cam.ac.uk/projects/relocalisation/]

The scripts in this folder are copied and modified from [Kent Sommer](https://github.com/kentsommer/tensorflow-posenet)


#### To train and test the network: 
Use **train.py** and **test.py**. Specifications of datasets and hyperparameters are defined in each file.

Use **evaluate_results.py** to evaluate the results from testing. 

#### To tune the beta factor: 
Use **beta_tuning.py**