import h5py
import numpy as np
import random
from tqdm import tqdm
from posenet import helper
import pickle
from scipy.misc import imsave
import cv2
import os, errno

""""
Generates train and test data sets of desired sizes. Input and out files are in hdf format.  
"""

# ------------------------------- Data set properties ---------------------------------------------
""" Define size of data set, set ID and the directory and name of input file here.  """
train_size = 5000
test_size = 1388
ID = 55
keep_temporal = True #Keeps the order of the original dataset in the train and test set if true, random if false
dir = '../hdf_data/'
data_file = 'corrected_april_7_labeled.h5'
track_dir = '../tracking_format/'


""" Name of output files (default) """
prefix = 'set#{}_'.format(ID)
train_file = 'train_data.h5'
test_file = 'test_data.h5'
# ---------------------------------------------------------------------------------------------------


def main():
    with h5py.File(dir + data_file, 'r') as hf:
        print("Creates hdf files...")
        data_size = len(hf.keys()) - 1
        vec = random.sample(range(data_size), train_size + test_size)
        if keep_temporal:
            vec = np.arange(0, train_size + test_size)
        train_vec = vec[:train_size]
        test_vec = vec[train_size:]
        with h5py.File(dir + prefix + train_file, 'w') as train:
            images = []
            for i, j in enumerate(tqdm(train_vec)):
                data = hf["data" + str(j)]
                data_set = train.create_dataset('data' + str(i), data=data)
                data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
            train.close()
        print("---------------------------Training done ---------------------------------")
        with h5py.File(dir + prefix + test_file, 'w') as test:
            for i, j in enumerate(tqdm(test_vec)):
                data = hf["data" + str(j)]
                data_set = test.create_dataset('data' + str(i), data=data)
                data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
            test.close()

        print("Successfully created set#{}, with train size {} and test size {}".format(ID, train_size, test_size))
        return

def filter_by_distance(limit):
    """ creates dataset including only datapoints with distances less than limit """

    with h5py.File(dir + data_file, 'r') as hf:
        print("Creates hdf files...")
        data_size = len(hf.keys()) - 1
        vec = random.sample(range(data_size), train_size + test_size)
        if keep_temporal:
            vec = np.arange(0, train_size + test_size - 1)
        train_vec = vec[:train_size]
        test_vec = vec[train_size:]
        index = 0
        with h5py.File(dir + prefix + train_file, 'w') as train:
            images = []
            for i, j in enumerate(tqdm(train_vec)):
                data = hf["data" + str(j)]
                pos = data.attrs["pose"][0:3]
                if np.linalg.norm(pos) < limit:
                    data_set = train.create_dataset('data' + str(index), data=data)
                    data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
                    index += 1
            train_length = len(train.keys())
            train.close()
        print("---------------------------Training done ---------------------------------")
        index = 0
        with h5py.File(dir + prefix + test_file, 'w') as test:
            for i, j in enumerate(tqdm(test_vec)):
                data = hf["data" + str(j)]
                pos = data.attrs["pose"][0:3]
                if np.linalg.norm(pos) < limit:
                    data_set = test.create_dataset('data' + str(index), data=data)
                    data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
                    index += 1
            test_length = len(test.keys())
            test.close()

        print("Successfully created set#{}, with train size {} and test size {}".format(ID, train_length, test_length))
        return



def merge(ID1, ID2, ID_res):
    """ Merges to existing datasets """
    prefix1 = 'set#{}_'.format(ID1)
    prefix2 = 'set#{}_'.format(ID2)
    prefix_res = 'set#{}_'.format(ID_res)

    i = 0
    with h5py.File(dir + prefix_res + train_file, 'w') as train:
        with h5py.File(dir + prefix1 + train_file, 'r') as train1:
            for j in tqdm(range(len(train1.keys()))):
                data = train1["data" + str(j)]
                pos = data.attrs["pose"][0:3]
                data_set = train.create_dataset('data' + str(i), data=data)
                data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
                i += 1
            train1.close()

        with h5py.File(dir + prefix2 + train_file, 'r') as train2:
            for j in tqdm(range(len(train2.keys()))):
                data = train2["data" + str(j)]
                pos = data.attrs["pose"][0:3]
                data_set = train.create_dataset('data' + str(i), data=data)
                data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
                i += 1
            train2.close()
        train_length = len(train.keys())
        train.close()

    i = 0
    with h5py.File(dir + prefix_res + test_file, 'w') as test:
        with h5py.File(dir + prefix1 + test_file, 'r') as test1:
            for j in tqdm(range(len(test1.keys()))):
                data = test1["data" + str(j)]
                pos = data.attrs["pose"][0:3]
                data_set = test.create_dataset('data' + str(i), data=data)
                data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
                i += 1
            test1.close()

        with h5py.File(dir + prefix2 + test_file, 'r') as test2:
            for j in tqdm(range(len(test2.keys()))):
                data = test2["data" + str(j)]
                pos = data.attrs["pose"][0:3]
                data_set = test.create_dataset('data' + str(i), data=data)
                data_set.attrs.create("pose", data.attrs["pose"], shape=(7,))
                i += 1
            test2.close()
        test_length = len(test.keys())
        test.close()

        string = "Successfully merged dataset #{} and #{}, and created dataset #{} of train size {} and " \
                 "test size {}".format(ID1, ID2, ID_res, train_length, test_length)
        print(string)


def to_pickle():
    """
    writes data object to pickle file
    """
    print("Creates pickle file...")
    dataset_train, dataset_test = helper.get_train_and_test(dir, prefix + train_file, prefix + test_file)
    tot = dataset_train, dataset_test
    tot = [dataset_train.images, dataset_train.poses, dataset_test.images, dataset_test.poses]
    with open(dir + prefix + '.pkl', 'wb') as f:
        pickle.dump(tot, f)
        f.close()
    print("created pickle file with data set ID#{}".format(ID))
    return

def to_aruco_tracker():
    """
    Writes test data to file format supported by the tracker.py algorithm:
    """
    filenames = []
    translation_vectors = []
    quaternions = []
    index = 0
    tracking_data = "tracking_data{}/".format(ID)

    if not os.path.exists(os.path.dirname(track_dir + tracking_data)):
        try:
            os.makedirs(os.path.dirname(track_dir + tracking_data))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise


    with h5py.File(dir + data_file, 'r') as hf:
        data_size = len(hf.keys()) - 1
        vec = random.sample(range(data_size), train_size + test_size)
        if keep_temporal:
            vec = np.arange(0, train_size + test_size)
        test_vec = vec[train_size:]

        for i, j in enumerate(tqdm(test_vec)):
            data = hf["data" + str(j)]
            pose = data.attrs["pose"]
            #data_set = test.create_dataset('data' + str(i), data=data)
            #data_set.attrs.create("pose", , shape=(7,))
            filename = "data_{}.png".format(index)
            translation_vectors.append(pose[:3])
            quaternions.append(pose[3:])
            filenames.append(filename)
            imsave(track_dir + tracking_data + filename, data)

            index+=1


    results = {'ID': filenames,
               'DCM': [],
               'tvec': translation_vectors,
               'rvec': quaternions}

    with open(track_dir + 'tracking_data{}.pkl'.format(ID), 'wb') as f:
        pickle.dump(results, f)


if __name__ == '__main__':
    main()










