# Data Processing
#### To obtain a labeled dataset (with images and relative poses) from bag files, run: 
1.  **bag_to_hdf.py** to write the bag files to hdf format. 
2.  **match.py** to match each image with a pose, and calculate relative pose between the ROV camera and target object. 
3. **gen_train_test.py** to split labeled dataset into train and test sizes

See description in each file where to define input/output files etc. 

#### Additional files 
**check_image.py** : plots and saves some sample images in png format 

**check_pose.py**: plots the pose labels for a dataset stored in hdf format

**check_sim_pose.py**: plots the pose labels for a dataset stored in pickle format

**pickler.py**: writes pickle file from python 3 to python 2 supported format 


