import numpy as np
import h5py
import cv2
import random
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle
from tqdm import tqdm
import os, errno



def centeredCrop(img, output_side_length):
	""" Crop images at center"""
	height, width, depth = img.shape
	new_height = output_side_length
	new_width = output_side_length
	if height > width:
		new_height = output_side_length * height / width
	else:
		new_width = output_side_length * width / height
	height_offset = int((height - new_height) / 2)
	width_offset = int((width - new_width) / 2)
	cropped_img = img[height_offset:height_offset + output_side_length, width_offset:width_offset + output_side_length]
	return cropped_img

def preprocess_pad_png(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):

		X = cv2.imread(images[i])
		pad = int((X.shape[1]-X.shape[0])/2)
		X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		X = cv2.resize(X, (256, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped


def preprocess_png(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):

		X = cv2.imread(images[i])
		#pad = int((X.shape[1]-X.shape[0])/2)
		#X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		#X = cv2.resize(X, (256, 256))
		#X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped



class Datasource(object):
	def __init__(self, images, poses):
		self.images = images
		self.poses = poses


def preprocess_pad(images):
	""" Crop and resize images, adds padding to keep entire image"""
	#TODO: consider normalization by mean, median
	images_cropped = []
	for i in range(len(images)):
		X = images[i]
		pad = int((X.shape[1]-X.shape[0])/2)
		X = cv2.copyMakeBorder(X, pad, pad, 0, 0, cv2.BORDER_CONSTANT)
		X = cv2.resize(X, (256, 256))
		X = centeredCrop(X, 224)
		images_cropped.append(X)
	return images_cropped

def get_data_batch_from_pickle(directory, picklefile, start_index, batch_size):
	poses = []
	images = []

	with open(directory + picklefile, 'rb') as f:
		res = pickle.load(f)
		f.close()

	img_dir = 'tracking_data/'
	for i in range(start_index, start_index + batch_size):
		fname = res['ID'][i]
		pos = np.array(res['tvec'][i])
		orient = np.array(res['rvec'][i])
		pose = np.concatenate((pos, orient))
		poses.append(pose)
		images.append(directory + img_dir + fname)
	images = preprocess_pad_png(images)
	return Datasource(images, poses)

# -------------------------- HDF -------------------------------
set = 40
no = 10

#hf = h5py.File('../hdf_data/set#2_train_data.h5', 'r')
hf = h5py.File('../hdf_data/set#{}_train_data.h5'.format(set), 'r')
size = len(hf.keys())

vec = random.sample(range(0, size), no)
dir= 'images/set{}/'.format(set)
if not os.path.exists(os.path.dirname(dir)):
	try:
		os.makedirs(os.path.dirname(dir))
		print("Made directory")
	except OSError as exc:  # Guard against race condition
		if exc.errno != errno.EEXIST:
			raise

for j, i in enumerate(vec):
	instance = hf["data{}".format(i)]
	img = instance[()]
	preprocessed = preprocess_pad([img])[0]
	pose = instance.attrs["pose"]
	plt.figure(j)
	#plt.subplot(121)
	#plt.imshow(img)
	#plt.xlabel("raw image")
	#plt.subplot(122)
	plt.imshow(preprocessed)
	plt.xlabel("preprocessed image")
	plt.suptitle("image{}".format(i))
	plt.savefig(dir + 'image{}.png'.format(i), format='png', dpi=1000)


# --------------------------- PICKLE ------------------------------
# batch_size = 20
# text_directory = './poselab/docker/resources/'
# text_file = 'tracking_data_py2.pkl'
# k = 1
# datasource = get_data_batch_from_pickle(text_directory, text_file, k * batch_size, batch_size)
# for i, image in enumerate(tqdm(datasource.images)):
# 	plt.figure(i)
#  	plt.imshow(image)
#  	plt.xlabel("processed image")
#  	plt.suptitle("image{}".format(i))
#  	plt.savefig(dir + 'image{}.png'.format(i), format='png')