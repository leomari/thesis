import pickle

""" Writes a pickle file from python 3 supported format to python 2 supported format. Must be run with python3. """

with open('tracking_data.pkl', 'rb') as f:
    res = pickle.load(f)
    f.close()

#with open('tracking_data_py2.pkl', 'wb') as f:
#    pickle.dump(res, f, protocol=2)
#    f.close()
print(len(res['ID']))
train_size = 1600
train = {}
test = {}

train['ID'] = res['ID'][0:train_size]
train['rvec'] = res['rvec'][0:train_size]
train['tvec'] = res['tvec'][0:train_size]

test['ID'] = res['ID'][train_size:]
test['rvec'] = res['rvec'][train_size:]
test['tvec'] = res['tvec'][train_size:]

print(len(test['ID']))
print(len(train['ID']))



with open('train_py3.pkl', 'wb') as f:
    pickle.dump(train, f, protocol=2)
    f.close()

with open('test_py3.pkl', 'wb') as f:
    pickle.dump(test, f, protocol=2)
    f.close()



#for i in range(10):
#    print(res['rvec'][i])