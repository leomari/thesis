import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle


out_file = 'plots/sim_data.eps'
pickle_file = './poselab/docker/resources/tracking_data.pkl'


def eul_from_quat(quat):
    """ Transforms orientation from quaternion to euler """
    w, x, y, z = quat
    roll = np.arctan2(2 * (w * x + y * z), 1 - 2 * (x ** 2 + y ** 2))
    pitch = np.arcsin(2 * (w * y - z * x))
    yaw = np.arctan2(2 * (w * z + x * y), 1 - 2 * (y ** 2 + z ** 2))
    return np.array([roll, pitch, yaw])

with open(pickle_file, 'rb') as f:
    res = pickle.load(f)
    f.close()

size = len(res['ID'])
rotations = res['rvec']
translations = res['tvec']
values = np.zeros((size, 6))

for i in range(size):
    pos = translations[i]
    rot = eul_from_quat(rotations[i])*180/np.pi
    values[i, :] = np.concatenate((pos, rot))


"""


with h5py.File('../hdf_data/corrected_april_6_labeled.h5', 'r') as hf:

    if template:
        temp = hf["temp_pose"]
    elif lab:
        temp = hf
    else:
        temp = hf["pose_data/poses"]

    print(len(temp.keys()))

    size = len(temp.keys())
    values = np.zeros((size,  6))
    for i in tqdm.tqdm(range(size-1)):
        if template:
            pose = temp["temp" + str(i)]
            pos = pose[()][:3]
            rot = match.DataMatcher.eul_from_quat(pose[()][3:7]) * 180 / np.pi
        elif lab:
            pose = temp["data" + str(i)]
            pos = pose.attrs["pose"][:3]
            rot = match.DataMatcher.eul_from_quat(pose.attrs["pose"][3:7]) * 180 / np.pi
        else:
            pose = temp["pose" + str(i)]
            pos = pose[()][:3]
            rot = match.DataMatcher.eul_from_quat(pose[()][3:7])*180/np.pi
        values[i, :] = np.concatenate((pos, rot))
    print(len(np.median(values, axis=0)))
    print(len(np.median(values, axis=1)))
    hf.close()


with h5py.File('../hdf_data/corrected_april_7_labeled.h5', 'r') as hf:

    if template:
        temp = hf["temp_pose"]
    elif lab:
        temp = hf
    else:
        temp = hf["pose_data/poses"]

    print(len(temp.keys()))

    size2 = len(temp.keys())
    values2 = np.zeros((size2,  6))

    for i in tqdm.tqdm(range(size2 - 1)):
        if template:
            pose = temp["temp" + str(i)]
            pos = pose[()][:3]
            rot = match.DataMatcher.eul_from_quat(pose[()][3:7]) * 180 / np.pi
        elif lab:
            pose = temp["data" + str(i)]
            pos = pose.attrs["pose"][:3]
            rot = match.DataMatcher.eul_from_quat(pose.attrs["pose"][3:7]) * 180 / np.pi
        else:
            pose = temp["pose" + str(i)]
            pos = pose[()][:3]
            rot = match.DataMatcher.eul_from_quat(pose[()][3:7])*180/np.pi
        values2[i, :] = np.concatenate((pos, rot))

    values = np.append(values, values2, axis=0)
    print(len(np.median(values, axis=0)))
    print(len(np.median(values, axis=1)))
    hf.close()
    size = size + size2
    
    
"""

plt.figure(1, figsize=(15, 15))
plt.title("Template pose")
plt.subplot(2,1,1)
plt.plot(values[:, 0], label='x')
plt.plot([np.mean(values[:, 0])]*size, label='x median')
plt.plot(values[:,1], label = 'y')
plt.plot([np.mean(values[:, 1])]*size, label='y median')
plt.plot(values[:,2], label='z')
plt.plot([np.mean(values[:, 2])]*size, label='z median')
plt.legend()
plt.xlabel("Samples")
plt.ylabel("[m]")
plt.title("Relative translations")

plt.subplot(2,1,2)

plt.plot(values[:, 4], label = 'pitch')
plt.plot([np.mean(values[:, 4])]*size, label='pitch median')
plt.plot(values[:, 5], label = 'yaw')
plt.plot([np.mean(values[:, 5])]*size, label='yaw median')
plt.plot(values[:, 3], label='roll')
plt.plot([np.mean(values[:, 3])]*size, label='roll median')
plt.legend()
plt.xlabel("Samples")
plt.ylabel("[deg]")
plt.title("Relative orientations")
plt.savefig(out_file, format='eps', dpi=1000)
#