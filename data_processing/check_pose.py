import h5py
import numpy as np
import tqdm
import match
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


#----------- Specify hdf-file and output file ----------
out_file = 'plots/corrected_data_5.eps'
hdf_file = '../hdf_data/corrected_april_6_labeled.h5'

#-default
template = False
lab = True


with h5py.File(hdf_file, 'r') as hf:

    if template:
        temp = hf["temp_pose"]
    elif lab:
        temp = hf
    else:
        temp = hf["pose_data/poses"]

    print(len(temp.keys()))

    size = len(temp.keys())
    values = np.zeros((size,  6))
    for i in tqdm.tqdm(range(size-1)):
        if template:
            pose = temp["temp" + str(i)]
            pos = pose[()][:3]
            rot = match.DataMatcher.eul_from_quat(pose[()][3:7]) * 180 / np.pi
        elif lab:
            pose = temp["data" + str(i)]
            pos = pose.attrs["pose"][:3]
            rot = match.DataMatcher.eul_from_quat(pose.attrs["pose"][3:7]) * 180 / np.pi
        else:
            pose = temp["pose" + str(i)]
            pos = pose[()][:3]
            rot = match.DataMatcher.eul_from_quat(pose[()][3:7])*180/np.pi
        values[i, :] = np.concatenate((pos, rot))
    print(len(np.median(values, axis=0)))
    print(len(np.median(values, axis=1)))
    hf.close()




plt.figure(1, figsize=(15, 15))
plt.title("Template pose")
plt.subplot(2,1,1)
plt.plot(values[:, 0], label='x')
plt.plot([np.mean(values[:, 0])]*size, label='x median')
plt.plot(values[:,1], label = 'y')
plt.plot([np.mean(values[:, 1])]*size, label='y median')
plt.plot(values[:,2], label='z')
plt.plot([np.mean(values[:, 2])]*size, label='z median')
plt.legend()
plt.xlabel("Samples")
plt.ylabel("[m]")
plt.title("Relative translations")

plt.subplot(2,1,2)
plt.plot(values[:, 3], label='roll')
plt.plot([np.mean(values[:, 3])]*size, label='roll median')
plt.plot(values[:, 4], label = 'pitch')
plt.plot([np.mean(values[:, 4])]*size, label='pitch median')
plt.plot(values[:, 5], label = 'yaw')
plt.plot([np.mean(values[:, 5])]*size, label='yaw median')
plt.legend()
plt.xlabel("Samples")
plt.ylabel("[deg]")
plt.title("Relative orientations")
plt.savefig(out_file, format='eps', dpi=1000)
#