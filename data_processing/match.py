import numpy as np
import h5py
import tqdm
# coding: utf8

"""
This script matches each image with its corresponding pose data point. Images are sampled at frequency 15 Hz, poses are 
sampled at 25 Hz. This implies maximum time difference between an image and its corresponding pose data point should be 
0.02 sec, which is set to be the default accepted time difference in the variable limit.  

The script calculates the position of the origin of the aruco board on the template box relative to the camera on the 
rov. Both frames are defined in NED, with the x-axis pointing forward on the rov and towards the control room on the 
template box. That is, the relative rotation between the camera and the aruco board is zero when the rov is placed 
right in front of the board. 

The script creates DataMatcher instance, taking the name of the raw hdf file (generated with bag_to_hdf.py) and the 
name of the resulting labeled hdf file as input. The position of the camera on bluerov2, and the position of the origin 
of the Aruco board on the template are written directly into the init method. The median of the template poses in the 
input file is used for calculation of the relative position. 

Both input and output files are in hdf format. The labeled output file is structured as follows: 

median of template poses: out_file["temp_pose/temp0"]
image: outfile["dataX"] with the relative pose of each image store in the attribute "pose". 

Define relative distances of object on template in init-method: self.camera_origin and self.template_origin

"""


# --------------------------- File names -------------------------------------
"""Input file """
in_file = '../hdf_data/april_2_raw.h5'
""" Output file """
out_file = '../hdf_data/corrected_april_2_labeled.h5'

filter_outliers = False

class DataMatcher():

    def __init__(self, raw_file, lab_file, img_path='/image_data/images/', pose_path='/pose_data/poses/', limit = 0.02):
        self.raw_file = raw_file
        self.lab_file = lab_file
        self.img_path = img_path
        self.pose_path = pose_path
        #self.camera_origin = np.array([0.37, 0.184, 0.04]) #lab1
        self.camera_origin = np.array([0.15, 0.18, 0.08 ]) # lab 2
        #self.template_origin = np.array([-0.015, -0.299, 0.31]) # lab 1
        #self.template_origin = np.array([0.015, -0.50, 0.17]) # lab 2 ring on box
        self.template_origin = np.array([0.015, -0.45, -0.29]) #lab 2 ring on stick
        self.limit = limit
        self.prev_pose = None
        self.prev_prev_pose = None

        with h5py.File(raw_file, 'r') as hf:
            temp = hf["temp_pose"]
            size = len(temp.keys())
            values = np.zeros((size, 7))
            for i in tqdm.tqdm(range(size)):
                pose = temp["temp" + str(i)]
                pos = pose[()][:3]
                rot = pose[()][3:7]
                values[i, :] = np.concatenate((pos, rot))
            self.temp_pose = np.median(values, axis=0)


    def time_diff(self, hdf, img_index, pose_index):
        img_time = hdf[self.img_path + "img" + str(img_index)].attrs["time"]
        pose_time = hdf[self.pose_path + "pose" + str(pose_index)][7]
        return img_time - pose_time

    def time_match(self, hdf, img_index, pose_index):
        if abs(self.time_diff(hdf, img_index, pose_index)) > self.limit:
            return False
        return True

    def find_start(self, hdf):
        img_i = 0
        pose_i = 0
        while self.time_diff(hdf, img_i, pose_i) > self.limit:
            # image_set starts later
            pose_i+=1
        while self.time_diff(hdf, img_i, pose_i) < -self.limit:
            # pose_set starts later
            img_i += 1
        return img_i, pose_i

    def find_end(self, hdf):
        img_i = len(hdf[self.img_path].keys())-1
        pose_i = len(hdf[self.pose_path].keys())-1
        print(img_i, pose_i)
        while self.time_diff(hdf, img_i, pose_i) > self.limit:
            img_i -= 1
        while self.time_diff(hdf, img_i, pose_i) < -self.limit:
            # pose_set ends later
            pose_i -= 1
        return img_i, pose_i

    # ------------------------------------ functions for calculating relative pose ------------------------------------
    @staticmethod
    def quat_from_eul(eul):
        """ Transforms orientation from euler to quaternion """
        cr = np.cos(0.5*eul[0])
        sr = np.sin(0.5*eul[0])
        cp = np.cos(0.5*eul[1])
        sp = np.sin(0.5*eul[1])
        cy = np.cos(0.5*eul[2])
        sy = np.sin(0.5*eul[2])

        x = cy*cp*sr - sy*sp*cr
        y = sy*cp*sr + cy*sp*cr
        z = sy*cp*cr - cy*sp*sr
        w = cy*cp*cr + sy*sp*sr

        return np.array([w, x, y, z])

    @staticmethod
    def eul_from_quat(quat):
        """ Transforms orientation from quaternion to euler """
        w, x, y, z = quat
        roll = np.arctan2(2*(w*x + y*z), 1-2*(x**2 + y**2))
        pitch = np.arcsin(2*(w*y - z*x))
        yaw = np.arctan2(2*(w*z + x*y), 1-2*(y**2 + z**2))
        return np.array([roll, pitch, yaw])

    @staticmethod
    def q_inv(q):
        """ Inverts a Quaternion """
        assert q.size == 4

        q[1:] = -q[1:]
        q_inv = q / np.linalg.norm(q)

        return q_inv

    @staticmethod
    def smtrx(x):
        """ Setup a Skew-Symmetrix Matrix """
        assert x.size == 3
        return np.array([
            [0, -x[2], x[1]],
            [x[2], 0, -x[0]],
            [-x[1], x[0], 0]])

    @staticmethod
    def quaternion_multiplication(a, b):
        """ Calculates the quaternion product between two quaternions """
        a0 = a[0]
        a1 = a[1:]
        b0 = b[0]
        b1 = b[1:]

        v0 = a0 * b0 - np.dot(a1, b1)
        v1 = a0 * b1 + a1 * b0 - np.dot(DataMatcher.smtrx(a1), b1)

        return np.insert(v1, 0, v0)

    @staticmethod
    def rel_rotation(quat_a, quat_b):
        """ rotation of frame a relative to frame b: resulting rotation applied to frame b gives frame a"""
        return DataMatcher.quaternion_multiplication(DataMatcher.q_inv(quat_b), quat_a)

    @staticmethod
    def rot_matrix_from_quaternion(quaternion):
        """ returns rotation matrix from body to ned: R_b^n """
        n, e1, e2, e3 = quaternion
        rot_matrix = np.array([[1-2*(e2**2 + e3**2), 2*(e1*e2 - e3*n), 2*(e1*e3 + e2*n)],
                               [2*(e1*e2 + e3*n), 1 - 2*(e1**2 + e3**2), 2*(e2*e3 - e1*n)],
                               [2*(e1*e3 - e2*n), 2*(e2*e3 + e1*n), 1 - 2*(e1**2 + e2**2)]])
        return rot_matrix

    def rel_pose(self, pose_a, pose_b):
        """
        defines the pose of object A(template) relative to a fixed object B (rov), expressed in the frame of
        object B. Orientation of input expressed in quaternions [w, x, y, z].
        """
        pos_a = pose_a[:3]
        pos_b = pose_b[:3]
        quat_a = pose_a[3:7]
        quat_b = pose_b[3:7]

        R_a = self.rot_matrix_from_quaternion(quat_a)
        R_b = self.rot_matrix_from_quaternion(quat_b)

        pos_rel = np.dot(R_b.transpose(), pos_a + np.dot(R_a, self.template_origin) - pos_b) - self.camera_origin
        #pos_rel = np.dot(R_a.transpose(), (pos_b + np.dot(R_b, self.camera_origin) - pos_a)) - self.template_origin
        quat_rel = matcher.rel_rotation(quat_a, quat_b)
        print((R_a.transpose()*pos_b).size)
        print(pos_rel.size, quat_rel.size, pos_rel, quat_rel)

        return np.concatenate((pos_rel, quat_rel))


    @staticmethod
    def diff(prev_prev_pose, prev_pose, pose, filter_outliers=filter_outliers):
        """ Filter outliers according to criterions below if filter_outliers is set to True """
        if not filter_outliers:
            return True
        assert len(pose) == 6
        #if np.any(abs(prev_pose[:3] - pose[:3]) > 5.0):
            #return False
        if np.any(abs(prev_pose[3:] - pose[3:]) > 10*np.pi/180) or np.any(abs(prev_prev_pose[3:] - pose[3:]) > 10*np.pi/180):
            return False
        elif np.any(abs(prev_pose[:3] - pose[:3]) > 0.10) or np.any(abs(prev_prev_pose[:3] - pose[:3]) > 0.10) :
            return False
        elif np.any(abs(pose[3:]) > 50*np.pi/180):
            return False
        elif np.any(abs(pose[:3]) > 1.3):
            return False
        else:
            return True

    @staticmethod
    def quat_pose_to_eul_pose(pose):
        eul_angle = DataMatcher.eul_from_quat(pose[3:7])
        return np.concatenate((pose[:3], eul_angle))


    # -------------------------------------- end functions for calculating relative pose ------------------------

    def match(self):
        with h5py.File(self.raw_file, 'r') as hf:
            with h5py.File(self.lab_file, 'w') as nf:
                img_start, pose_start = self.find_start(hf)
                img_end, pose_end = self.find_end(hf)


                index = 0
                disc_pos = []
                disc_img = []
                img_i, pose_i = img_start, pose_start

                data_set = nf.create_dataset(
                    name="temp_pose/temp0",
                    data=self.temp_pose,
                    shape=self.temp_pose.shape
                )

                while img_i < img_end:

                    for j in tqdm.tqdm(range(pose_i, pose_end+1)):
                        print("iteration pose :{}, for image {}".format(j, img_i))

                        if j == pose_i + 5 or j == pose_end: #discards image
                            print("discarded image{}".format(img_i))
                            disc_img.append(img_i)
                            img_i += 1
                            break
                        if self.time_match(hf, img_i, j): #matches images and pose

                            rel_pose = self.rel_pose(self.temp_pose, hf[self.pose_path + "pose" + str(pose_i)][:7])
                            rel_pose_eul = DataMatcher.quat_pose_to_eul_pose(rel_pose)
                            if index == 0:
                                self.prev_pose = rel_pose_eul
                                self.prev_prev_pose = rel_pose_eul

                            if DataMatcher.diff(self.prev_prev_pose, self.prev_pose, rel_pose_eul):

                                print("matched pose {} with image {}".format(pose_i, img_i))

                                pose_i = j
                                img = hf[self.img_path + "img" + str(img_i)].value
                                data_set = nf.create_dataset(
                                    name="data" + str(index),
                                    data=img,
                                    shape=img.shape
                                    )
                                data_set.attrs.create("pose", rel_pose, shape=(7,))
                                index += 1
                                img_i += 1
                            self.prev_prev_pose = self.prev_pose
                            self.prev_pose = rel_pose_eul
                            #todo: change temp_pose variable if varying/mean


                            pose_i += 1


                            break
                        else: #discards pose
                            disc_pos.append(j)
                            disc_ammount = len(disc_pos)/max(1,float(pose_i - pose_start))*100
                            print("discarded pose {0}, total discarded {1:.0f} percentage of pose data".format(j, disc_ammount))
            print("discarded {} poses and {} images".format(len(disc_pos), len(disc_img)))
            #print("Length of labeled dataset: {}".format(len(nf.keys())))
            nf.close()
        hf.close()

        with h5py.File(self.lab_file, 'r') as nf:
            print("Length of labeled dataset: {}".format(len(nf.keys())))
            print(img_start, pose_start, img_end, pose_end)
        nf.close()


if __name__ == '__main__':
    matcher = DataMatcher(in_file, out_file)
    matcher.match()