import rosbag
from qualisys.msg import Subject
import h5py
import cv2
import numpy as np
from tqdm import tqdm
from sensor_msgs.msg import CompressedImage


"""
Writes data from bag files to hdf files. Gets pose of bluerov2 and the template box from "pose_bag", and the images of 
the from "img_bag". Stores data in the hdf file "filename" in the following structure: 

images: filename["image_data/images/imgX"] with attribute "time" storing the time slot in seconds for each image
bluerov2 poses: filename["pose_data/poses/poseX"]
template box poses: filename["temp_pose/tempX"]

The poses are stored in 8-dimensional arrays, where the last entry is the time slot in seconds for the data point. 
"""

# ------------------------------ Define variables of input and output files -------------------------------------------
topics = ['/qualisys/bluerov2', '/bluerov2/sensors/raspicam/image_raw/compressed', '/qualisys/template']

"""Input files """


pose_bag = '../friday_third_run/bluerov2/_2019-02-01-13-21-48_0.bag'
img_bag = '../friday_third_run/bluerov2_image_2019-02-01-13-21-48_0.bag'


pose_bag = '../../mclab_april/bags/bluerov2/_2019-04-30-15-44-50_0.bag'
img_bag = '../../mclab_april/bags/bluerov2_image_2019-04-30-15-44-50_0.bag'



"Output file"
filename = '../hdf_data/april_9_raw.h5'
# ---------------------------------------------------------------------------------------------------------------------


def bag_to_hdf(filename, bagname, mode, temp_pose=False):
    with rosbag.Bag(bagname, 'r') as bag:
        with h5py.File(filename, mode) as hf:

            index = 0
            for topic, msg, t in tqdm(bag.read_messages(topics=topics)):
                if topic == topics[1]:
                    data = msg.data
                    np_arr = np.fromstring(data, np.uint8)
                    print('len(np.array): {}'.format(len(np_arr)))
                    image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
                    #print(t,t.to_sec(), type(t), type(t.to_sec()), image_np.shape, type(image_np))

                    img_set = hf.create_dataset(
                        name='image_data/images/img' + str(index),
                        data=image_np,
                        shape=image_np.shape)
                    img_set.attrs["time"] = t.to_sec()
                    print("added img{} to hdf".format(index))
                    index += 1

                elif topic == topics[0] and not temp_pose:
                    pos = [msg.position.x, msg.position.y, msg.position.z]
                    attitude = [msg.orientation.w, msg.orientation.x, msg.orientation.y, msg.orientation.z]
                    pose = np.concatenate((pos, attitude, [t.to_sec()]))
                    print("added pose{} to hdf".format(index))
                    print(pose)

                    pose_set = hf.create_dataset(
                        name='pose_data/poses/pose' + str(index),
                        data=pose,
                        shape=(8,)
                    )
                    index += 1

                elif topic == topics[2] and temp_pose:
                    pos = [msg.position.x, msg.position.y, msg.position.z]
                    attitude = [msg.orientation.w, msg.orientation.x, msg.orientation.y, msg.orientation.z]
                    pose = np.concatenate((pos, attitude, [t.to_sec()]))
                    #print("pose of template{}".format(index))
                    #print(pose)
                    temp_set = hf.create_dataset(
                        name='temp_pose/temp' + str(index),
                        data=pose,
                        shape=(8,)
                    )
                    index += 1
            print(index)
        hf.close()
    bag.close()


if __name__ == '__main__':
    bag_to_hdf(filename, pose_bag, 'w', temp_pose=True)
    bag_to_hdf(filename, pose_bag, 'r+')
    bag_to_hdf(filename, img_bag, 'r+')
