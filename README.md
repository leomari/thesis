# Underwater Pose Estimation with Deep Learning 

Repository for my mastes thesis "Underwater Pose Estimation with Deep Learning", submitted at NTNU

**data_processing/** contains necessary code for converting bag files to hdf files, and match images with pose labels

**tensorflow_posenet/** contains tensorflow implementation of the posenet CNN. 

